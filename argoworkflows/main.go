package main

import (
	"fmt"

	argoClient "github.com/argoproj/argo-workflows/v3/pkg/apiclient"
	cliWf "github.com/argoproj/argo-workflows/v3/pkg/apiclient/workflow"
)

func main() {
	opts := argoClient.Opts{
		ArgoServerOpts: argoClient.ArgoServerOpts{
			URL:    "HOST:PORT",
			Secure: true,
		},
		AuthSupplier: func() string {
			return "BEARER_TOKEN"
		}, // It'll be sending authorization header to argo server.
	}
	ctx, cli, err := argoClient.NewClientFromOpts(opts)
	if err != nil {
		fmt.Println(err)
		return
	}
	in := &cliWf.WorkflowCreateRequest{
		Namespace: "NAMESPACE", // At least, put the proper namespace here.
	}
	res, err := cli.NewWorkflowServiceClient().CreateWorkflow(ctx, in)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(res)
}
