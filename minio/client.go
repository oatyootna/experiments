package main

import (
	"context"
	"fmt"
	"log"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

func main() {
	endpoint := "xxxx"
	accessKeyID := "xxxxx"
	secretAccessKey := "xxxxx"
	useSSL := true

	// Initialize minio client object.
	client, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
	})
	if err != nil {
		log.Fatalln(err)
	}
	// log.Printf("%#v\n", client) // minioClient is now set up
	ctx := context.Background()
	buckets, _ := client.ListBuckets(ctx)
	for _, b := range buckets {
		fmt.Println(b)
	}
}
